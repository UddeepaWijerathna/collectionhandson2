package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {


    public static void main(String[] args) {
        List<Movie> movies = new ArrayList<Movie>(10);
        movies.add(new Movie(5, "DON", 2012));
        movies.add(new Movie(3, "Barbie", 2000));
        movies.add(new Movie(1, "The NoteBook", 2004));
        movies.add(new Movie(3.5, "Titanic", 1997));

        for(Movie movie: movies)
            System.out.println(movie);
        System.out.println("---------------------------------------");
        System.out.println("---------------  Sort By Rating  --------------------");

        SortByRating sortByRating= new SortByRating();
        Collections.sort(movies,sortByRating);
        //Collections.sort(movies,SortByRating);

        for(Movie movie: movies)
            System.out.println(movie);


        System.out.println("---------------  Sort By Name  --------------------");

        SortByName sortByName =new SortByName();
        Collections.sort(movies,sortByName);

        for(Movie movie: movies)
            System.out.println(movie);
    }
}
