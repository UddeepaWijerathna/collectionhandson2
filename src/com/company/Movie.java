package com.company;

public class Movie implements Comparable<Movie>{
    private double rating;
    private String name;
    private int releasedYear;

    public Movie(double rating, String name, int releasedyear) {
        this.rating = rating;
        this.name = name;
        this.releasedYear = releasedyear;

    }

    @Override
    public String toString() {
        return "Movie{" +
                "rating=" + rating +
                ", name='" + name + '\'' +
                ", releasedYear=" + releasedYear +
                '}';
    }

    @Override
    public int compareTo(Movie o) {
        if (this.releasedYear > o.releasedYear)
            return 1;
        else
            return 0;
    }

    public String getName() {
        return name;
    }

    public double getRating() {
        return rating;
    }
}
