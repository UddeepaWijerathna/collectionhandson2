package com.company;

import java.util.Comparator;

public class SortByRating implements Comparator<Movie> {
    @Override
    public int compare(Movie a, Movie b) {
        if (a.getRating()>b.getRating())
            return 1;
        else if(a.getRating()<b.getRating())
            return -1;
        else
            return 0;
    }
}
